Name:           ladspa
Version:        1.17
Release:        1
Summary:        Linux Audio Developer's Simple Plugin API (LADSPA)
License:        LGPLv2.1-only
URL:            http://www.ladspa.org/
Source:         http://www.ladspa.org/download/%{name}_sdk_%{version}.tgz
Patch0:         ladspa-1.17.patch
BuildRequires:  perl-interpreter gcc-c++

BuildRequires:  pkgconfig(sndfile)

%description
LADSPA is a standard that allows software audio processors and effects to
be plugged into a wide range of audio synthesis and recording packages.

%package        devel
Summary:        Linux Audio Developer's Simple Plug-in API
Requires:       %{name} = %{version}-%{release}

%description    devel
Thie package contains develop files and ladspa.h header file.

%prep
%autosetup -n ladspa_sdk_%{version} -p1
perl -pi -e 's/^(CFLAGS.*)-O2(.*)/$1\$\(RPM_OPT_FLAGS\)$2 -DDEFAULT_LADSPA_PATH=\$\(PLUGINDIR\)/' src/Makefile
perl -pi -e 's/-mkdirhier/-mkdir -p/' src/Makefile
cd doc|perl -pi -e "s!HREF=\"ladspa.h.txt\"!href=\"file:///usr/include/ladspa.h\"!" *.html

%build
%set_build_flags
cd src
PLUGINDIR=%{_libdir}/ladspa make targets %{?_smp_mflags} LD="ld --build-id"

%install
cd src
%make_install INSTALL_PLUGINS_DIR=$RPM_BUILD_ROOT%{_libdir}/ladspa \
              INSTALL_INCLUDE_DIR=$RPM_BUILD_ROOT%{_includedir} \
              INSTALL_BINARY_DIR=$RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_datadir}/ladspa/rdf

%files
%doc doc/COPYING
%{_datadir}/ladspa
%{_libdir}/ladspa/*.so
%{_bindir}/analyseplugin
%{_bindir}/applyplugin
%{_bindir}/listplugins

%files devel
%doc doc/*.html
%{_includedir}/ladspa.h

%changelog
* Thu Dec 26 2024 xu_ping <7070778654@qq.com> - 1.17-1
- Upgrade version to 1.17
  * Rename Makefile.
  * Modernise C++ #include style.
  * Make some globals static.
  * Use mkdir -p rather than mkdirhier during build.
  * Use GCC export map to ensure only the ladspa_descriptor() exported.
  * Put libraries at the end of link instructions.
  * Package with version number in archive and directory names.
  * Tweaks to documentation processing.
  * Fix bug in LADSPA plugin search which did not handle shared libraries that are not plugins correctly.
  * Introduce a default LADSPA plugin search path.
  * Perform macro string expansion in C code rather than in Makefile for better portability.
  * Modernise init()/fini() style in GNU C plugin builds (not C++), tweak link line to correspond.
  * Change applyplugin to use libsndfile.
  * Fix URL in documentation.
  * Fix so delay can handle a delay of zero.
  * Simplify, and hopefully accelerate, clip checking code in applyplugin.
  * Improved usage message.

* Wed Sep 1 2021 Zhengtang Gong <gongzhengtang@huawei.com> - 1.13-23
- add bind now

* Fri Nov 8 2019 Yiru Wang <wangyiru1@huawei.com> - 1.13-22
- Pakcage init
